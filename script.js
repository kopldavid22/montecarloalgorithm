const tryNumbers = 100000;
let array = [[], []]; //  100 hodnot
let randomX, randomY; // -1 <-> 1
let arrayPossition = true;

const possibleNegace = (number) => {
  if (arrayPossition) {
    if (Math.random() < 0.5) {
      array[0].push((number = number - 1));
      // console.log(number);
      return number;
    } else {
      // console.log(number);
      array[0].push(number);
      return number;
    }
  } else {
    if (Math.random() <= 0.5) {
      array[1].push((number = number - 1));
      // console.log(number);
      return number;
    } else {
      // console.log(number);
      array[1].push(number);
      return number;
    }
  }
  //true false / 50%
};

const random = () => {
  randomX = Math.random(); //0 do 1
  randomY = Math.random(); //0 do 1

  possibleNegace(randomX); // -1 - +1
  possibleNegace(randomY); // -1 - +1
};

for (let i = 0; i < tryNumbers; i++) {
  random();
  arrayPossition ? (arrayPossition = false) : (arrayPossition = true);
}
//Do této doby jsme naplnili pole[0] = 100 náhodnými čísly od 0-1, pole[1] -||-
// console.log(array);

let isInTheCircleCounter = 0;
let isNotInTheCircleCounter = 0;

function getDifference(a, b) {
  return Math.abs(a - b);
}

for (let j = 0; j < array[0].length; j++) {
  if (getDifference(array[0][j], array[1][j]) > 1) {
    isNotInTheCircleCounter++;
  } else {
    isInTheCircleCounter++;
  }
}

console.log(isInTheCircleCounter, isNotInTheCircleCounter);

const calcPi = (isIn, all) => {
  console.log(`Náš výsledek za pomoci nahody = ${isIn / all}`);
};
console.log('PI / 4');
console.log(`Vysledek z JS = ${Math.PI / 4}`);
calcPi(isInTheCircleCounter, tryNumbers);

const aproximace = (isIn, all) => {
  console.log(`Náš výsledek aproximace cisla PI = ${(isIn / all) * 4}`);
  console.log(`Vysledek JS cisla PI ${(Math.PI / 4) * 4}`);
};

aproximace(isInTheCircleCounter, tryNumbers);
